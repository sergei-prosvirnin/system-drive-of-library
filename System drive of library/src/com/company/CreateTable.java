package com.company;

import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;

public class CreateTable {

    public static void main(String Query) {

        final String DRIVER =
                "org.apache.derby.jdbc.EmbeddedDriver";
        final String CONNECTION =
                "jdbc:derby:AccountDatabase;create=true";


        try {
            Class.forName(DRIVER).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

      try (Connection connection =
              DriverManager.getConnection(CONNECTION);

           Statement statement =
              connection.createStatement()) {

        statement.executeUpdate(Query);

      } catch (SQLException e) {
            e.printStackTrace();
      }
    }
}


