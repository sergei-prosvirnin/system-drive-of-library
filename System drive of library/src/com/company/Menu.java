package com.company;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Scanner;
import java.util.Date;
import java.text.SimpleDateFormat;

import static java.lang.System.out;

public class Menu {
    //Control variables
    private int option;
    private boolean first_use;
    static String SqlDateReturn;

    //String Objects - System Messages

    //First level
    String Main_menu =
                    "\nДобро пожаловать в систему управления библиотекой ! " +
                    "Пожалуйста выберете с помощью цифр, с чем вы хотите работать.\n" +
                    "1. Справочник книг.\n" +
                    "2. Справочник читателей.\n" +
                    "3. Выдача/возврат книг.\n" +
                    "4. Об авторе.\n" +
                    "5. Выход из прораммы.\n" +
                    "Введите номер пункта";

    //Second level
    String Books_menu =
                    "1. Просмотерть справочник книг.\n" +
                    "2. Редактировать справочник книг.\n" +
                    "Введите номер пункта";
    String Readers_menu =
                    "1. Просмотерть справочник читателей.\n" +
                    "2. Редактировать справочник читателей.\n" +
                    "Введите номер пункта";
    String Extradition_menu =
                    "1. Выдать книгу.\n" +
                    "2. Принять книгу.\n" +
                    "3. Узнать взятые книги определённого читаталя.\n" +
                    "4. Показать читателей бравших определённую книгу.\n" +
                    "Введите номер пункта";
    String Autor_menu =
                    "Автор программы: Просвирнин Сергей Денисович\n" +
                    "E-mail: volgakey@gmail.com\n" +
                    "Телефон:8 (904) 266-18-19\n" +
                    "Введите любое число, что бы вернуться в главное меню.";

    //Third level
    String sub_BookAndReaders_menu =
                    "1. Добавить запись.\n" +
                    "2. Удалить запись.\n" +
                    "3. Редактировать запись.\n" +
                    "Введите номер пункта";

    //Create objects
    Scanner in = new Scanner(System.in);
    Validation validation=new Validation();

    void all_menu() {
        this.select(Main_menu, 5);
        this.menu();
    }//end all_menu

    void select(String Message, int ending) {
        if (option == 0) first_use = true;
        do {
            out.print(Message);
            option = validation.IntValid();
            out.print("\n\n\n");
        }//do-while
        while (option < 1 || option > ending);
        if (first_use == true && option == ending) System.exit(0);
    }//end select

    void menu() {

        switch (option) {

            //Start Finish_menu
            case 1:

                //Вызов меню работы с читателями
                this.select(Books_menu, 3);

                switch (option) {

                    case 1://Просмотр всех записей о книгах

                        GetData.main("SELECT * FROM BOOKS", 1);
                        break;

                    case 2://Манипуляции с БД

                        //Вызов меню работы с записями книг/читателей
                        this.select(sub_BookAndReaders_menu, 4);

                        switch (option) {

                            case 1://Добавление записи о книге

                                //Построение SQL запроса
                                out.println(
                                        "Добавление новой записи о книге (все пункты обязательны)" +
                                        "\n─────────────────────────────────────────────────────────────────────\n" +
                                        "Имя"
                                );
                                String NameBook = validation.StringValid();
                                out.println("Автор");
                                String AutorBook = validation.StringValid();
                                out.println("Издательский дом");
                                String PublishingHouseBook = validation.StringValid();
                                out.println("Год");
                                int AnnumBook = validation.IntValid();
                                out.println("Кол-во страниц");
                                int NumberPagesBook = validation.IntValid();
                                out.println("Цена");
                                int PriceBook = validation.IntValid();
                                out.println("Кол-во книг");
                                int AmountBook = validation.IntValid();
                                String INSERT_BOOKS =
                                 "INSERT INTO BOOKS (NAME,AUTOR,PUBLISHING_HOUSE,ANNUM,NUMBER_PAGES,PRICE,AMOUNT) VALUES (\n'" +
                                                NameBook + "',\n'" +
                                                AutorBook + "',\n'" +
                                                PublishingHouseBook + "',\n" +
                                                AnnumBook + ",\n" +
                                                NumberPagesBook + ",\n" +
                                                PriceBook + ",\n" +
                                                AmountBook + ")\n";

                                //Отправка SQL запроса
                                CreateTable.main(INSERT_BOOKS);
                                break;

                            case 2: //Удаление записи о книге

                                //Ввод библиотечного шифра
                                out.print("Введите библиотечный шифр книги, которую хотите удалить");
                                int DeleteLibraryCipher = validation.IntValid();

                                //Построение SQL запроса
                                String DELETE_BOOKS = "DELETE FROM BOOKS WHERE LIBRARY_CIPHER = " + DeleteLibraryCipher;

                                //Отправка SQL запроса
                                CreateTable.main(DELETE_BOOKS);
                                break;

                            case 3:  //Обновление записи о книге

                                //Ввод библиотечного шифра
                                out.print("Введите библотечный шифр книги, которую хотите отредактировать");
                                int UpdateLibraryCipher = validation.IntValid();

                                //Выбор редактируемой записи
                                out.print(
                                                "\nВыберите c помощью цифр запись которую хотите отредактировать" +
                                                "\n─────────────────────────────────────────────────────────────\n" +
                                                "1 - Имя\n" +
                                                "2 - Автор\n" +
                                                "3 - Издательский дом\n" +
                                                "4 - Год\n" +
                                                "5 - Кол-во страниц\n" +
                                                "6 - Цена\n" +
                                                "7 - Кол-во экземпляров\n"
                                );

                                int UpdateSelection = validation.UpdateMenuValid(7);

                                String UpdateString = null;

                                switch (UpdateSelection) {
                                    case 1:
                                        UpdateString = "NAME";
                                        break;
                                    case 2:
                                        UpdateString = "AUTORS";
                                        break;
                                    case 3:
                                        UpdateString = "PUBLISHING_HOUSE";
                                        break;
                                    case 4:
                                        UpdateString = "ANNUM";
                                        break;
                                    case 5:
                                        UpdateString = "NUMBER_PAGES";
                                        break;
                                    case 6:
                                        UpdateString = "PRICE";
                                        break;
                                    case 7:
                                        UpdateString = "AMOUNT";
                                        break;
                                }//end switch

                                //Ввод новой информации
                                out.print("Введите новую информацию");
                                String UpdateNew = validation.StringValid();

                                //Построение SQL запроса
                                String UpdateNewPaste=null;

                                if (UpdateString=="NAME"||UpdateString=="AUTORS"||UpdateString=="PUBLISHING_HOUSE") UpdateNewPaste="'"+UpdateNew+"'";
                                else UpdateNewPaste=UpdateNew;

                                String UPDATE_BOOKS = "UPDATE BOOKS SET " + UpdateString + " = " + UpdateNewPaste + " WHERE LIBRARY_CIPHER =  " + UpdateLibraryCipher;

                                //Построение SQL запроса
                                CreateTable.main(UPDATE_BOOKS);
                                break;

                        }//end sub_switch
                }//end switch
                break;
            //Finish Books_menu

           //Start Readers_menu
            case 2://Работата с читателями

                //Вызов меню работы с читателями
                this.select(Readers_menu, 3);

                switch (option) {

                    case 1: //Просмотр всех записей читателей

                        GetData.main("SELECT * FROM READERS", 2);
                        break;


                    case 2: //Манипуляции с БД

                        //Вызов меню работы с записями книг/читателей
                        this.select(sub_BookAndReaders_menu, 4);

                        switch (option) {

                            case 1://Добавление записи о читателе

                                //Построение SQL запроса
                                out.print(
                                        "Добавление новой записи о читателе (все пункты обязательны)" +
                                        "\n─────────────────────────────────────────────────────────────────────\n" +
                                        "Ф.И.О."
                                );
                                String FullNameReaders = validation.StringValid();
                                out.print("Домашний адрес");
                                String HomeAddressReaders = validation.StringValid();
                                out.print("Номер телефона");
                                int PhoneNumberReaders = validation.IntValid();;
                                String INSERT_READERS =
                                        "INSERT INTO READERS (FULL_NAME,HOME_ADDRESS,PHONE_NUMBER) VALUES (\n'" +
                                                FullNameReaders + "',\n'" +
                                                HomeAddressReaders + "',\n" +
                                                PhoneNumberReaders + "\n" +
                                                ")\n";

                                //Отправка SQL запроса
                                CreateTable.main(INSERT_READERS);
                                break;

                            case 2: //Удаление записи о читателе

                                //Выбор кода читателя
                                out.print("Введите код читателя, которого хотите удалить");
                                int DeleteReadersCode = validation.IntValid();

                                //Построение SQL запроса
                                String DELETE_READERS = "DELETE FROM READERS WHERE READERS_CODE = " + DeleteReadersCode;

                                //Отправка SQL запроса
                                CreateTable.main(DELETE_READERS);
                                break;

                            case 3: //Обновление записи о читателе

                                //Выбор кода читателя
                                out.print("Введите код читателя, которого хотите отредактировать");
                                int UpdateReadersCode = validation.IntValid();

                                //Выбор редактируемой записи
                                out.print(
                                                "\nВыберите c помощью цифр запись которую хотите отредактировать" +
                                                "\n─────────────────────────────────────────────────────────────\n" +
                                                "1 - Ф.И.О.\n" +
                                                "2 - Домашний адрес\n" +
                                                "3 - Номер телефона\n"
                                );
                                int UpdateSelection = validation.UpdateMenuValid(3);

                                String UpdateString  = null;

                                switch (UpdateSelection) {
                                    case 1:
                                        UpdateString = "FULL_NAME";
                                        break;
                                    case 2:
                                        UpdateString = "HOME_ADDRESS";
                                        break;
                                    case 3:
                                        UpdateString = "PHONE_NUMBER";
                                        break;
                                }//end switch

                                //Ввод новой информации
                                out.print("Введите новую информацию");
                                String UpdateNew=validation.StringValid();

                                //Построение SQL запроса
                                String UpdateNewPaste  = null;

                                if (UpdateString=="FULL_NAME"||UpdateString=="HOME_ADDRESS") UpdateNewPaste="'"+UpdateNew+"'";
                                else UpdateNewPaste=UpdateNew;

                                String UPDATE_READERS = "UPDATE READERS SET " + UpdateString + " = " + UpdateNewPaste + " WHERE READERS_CODE =  " + UpdateReadersCode;

                                //Отправка SQL запроса
                                CreateTable.main(UPDATE_READERS);
                                break;

                        }//end sub_switch
                }//end switch
                break;
            //Finish Readers_menu

            //Start Extradition_menu
            case 3: //Выдача книг

                //Вызов меню книг
                this.select(Extradition_menu, 5);

                switch (option){

                    case 1: //Выдать книгу

                        //Построение SQL запроса
                        out.print("Введите код читателя");
                        int ExtraditionReadersCode = validation.IntValid();
                        out.print("Введите библиотечный шифр");
                        String ExtraditionLibraryCipher = validation.StringValid();

                        //Ввод и валидация даты
                        String ExtraditionDate = validation.DateStirngValid();

                        String INSERT_ISSUING_BOOKS= "INSERT INTO ISSUING_BOOKS (DATE_ISSUE,DATE_RETURN,LIBRARY_CIPHER,READERS_CODE) VALUES (\n" +
                                "CURRENT_DATE,\n" + "'"+ExtraditionDate+"',\n"+ ExtraditionLibraryCipher+",\n" + ExtraditionReadersCode+"\n"+ ")";

                        //Отправка SQL запроса
                        CreateTable.main(INSERT_ISSUING_BOOKS);
                        break;

                    case 2: //Принять книгу

                        /*Принятие книги
                        **Построение SQL запроса*/
                        out.print("Введите код выдачи");
                        int ExtraditionIssueCode = validation.IntValid();

                        String UPDATE_ISSUING_BOOKS="UPDATE ISSUING_BOOKS SET RETURNED = TRUE WHERE ISSUE_CODE = " + ExtraditionIssueCode;

                        //Отправка SQL запроса

                        CreateTable.main(UPDATE_ISSUING_BOOKS);

                        /*Проверка на штраф
                        **Построение SQL запроса*/
                        boolean TestOnDelay = false;
                        String EXISTS_DELAY="SELECT DATE_RETURN FROM ISSUING_BOOKS WHERE ISSUE_CODE = " + ExtraditionIssueCode;
                        GetData.main(EXISTS_DELAY, 5);

                        //Непосредственная проверка на штраф
                        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date DATE_RETURN = sdf.parse(SqlDateReturn);
                            Date today=new Date();
                            TestOnDelay = today.after(DATE_RETURN);
                            if (TestOnDelay){
                                out.println("Книга просрочена ! Не забудьте взять штраф !");
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        break;

                    case 3: //Узнать взятые книги определённого читаталя

                        //Построение SQL запроса
                        out.println("Введите код читателя:");
                        int ExtraditionReadersOfBookCode = validation.IntValid();
                        String SELECT_BOOKS_OF_READERS="SELECT LIBRARY_CIPHER,NAME FROM ISSUING_BOOKS NATURAL JOIN BOOKS WHERE READERS_CODE = " + ExtraditionReadersOfBookCode;

                        //Отправка SQL запроса
                        GetData.main(SELECT_BOOKS_OF_READERS, 3);
                        break;

                    case 4: //Показать читателей бравших определённую книгу

                        //Построение SQL запроса
                        out.println("Введите код книги:");
                        int ExtraditionLibraryCodeOfBook = validation.IntValid();
                        String SELECT_READERS_WICH_TAKE="SELECT READERS_CODE,FULL_NAME FROM ISSUING_BOOKS NATURAL JOIN READERS WHERE LIBRARY_CIPHER = " + ExtraditionLibraryCodeOfBook;

                        //Отправка SQL запроса
                        GetData.main(SELECT_READERS_WICH_TAKE, 4);
                        break;

                };
                break;
            //Finish Extradition_menu
            case 4:
                out.print(Autor_menu);
                validation.IntValid();
                break;
            case 5:
                this.select(Main_menu, 5);
                break;
        }//end main_switch
    }//end menu
}//end class Menu
