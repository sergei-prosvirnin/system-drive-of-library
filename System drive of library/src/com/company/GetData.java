package com.company;

import static java.lang.System.out;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.text.NumberFormat;

public class GetData {

    public static void main(String Query, int select) {
        
        NumberFormat currency =
                NumberFormat.getCurrencyInstance();

        final String DRIVER =
                "org.apache.derby.jdbc.EmbeddedDriver";
        final String CONNECTION =
                "jdbc:derby:AccountDatabase";


        try {
            Class.forName(DRIVER).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection =
                     DriverManager.getConnection(CONNECTION);

             Statement statement =
                     connection.createStatement();

             ResultSet resultset =
                     statement.executeQuery(Query)) {
switch (select) {
    case 1:
        while (resultset.next()) {
            out.println("Название книги: " + resultset.getString("NAME"));
            out.println("──────────────────────────────────────────────────────────");
            out.println("Библиотечный шифр: " + resultset.getString("LIBRARY_CIPHER"));
            out.println("Автор: " + resultset.getString("AUTOR"));
            out.println("Издательский дом: " + resultset.getString("PUBLISHING_HOUSE"));
            out.println("Год: " + resultset.getInt("ANNUM"));
            out.println("Кол-во страниц: " + resultset.getString("NUMBER_PAGES"));
            out.println("Цена: " + resultset.getString("PRICE"));
            out.println("Кол-во экзепляров: " + resultset.getString("AMOUNT"));
            out.println("\n\n");
        }//end while
        break;
    case 2:
        while (resultset.next()) {
            out.println("Библиотечная карточка: " + resultset.getString("READERS_CODE"));
            out.println("\n─────────────────────────────────────────────────\n");
            out.println("Ф.И.О.: " + resultset.getString("FULL_NAME"));
            out.println("Домашний адрес: " + resultset.getString("HOME_ADDRESS"));
            out.println("Домашний телефон: " + resultset.getString("PHONE_NUMBER"));
            out.println("\n");
        }//end while
            break;
    case 3:
        while (resultset.next()) {
            out.println("Библиотечный шифр: " + resultset.getString("LIBRARY_CIPHER"));
            out.println("Имя: " + resultset.getString("NAME"));
            out.println("\n");
        }//end while
        break;
    case 4:
        while (resultset.next()) {
            out.println("Код читателя: " + resultset.getString("READERS_CODE"));
            out.println("Полное имя: " + resultset.getString("FULL_NAME"));
            out.println("\n");
        }//end while
        break;
    case 5:
        while (resultset.next()) {
            Menu.SqlDateReturn= resultset.getString("DATE_RETURN");
        }//end while
        break;

}//end switch
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
