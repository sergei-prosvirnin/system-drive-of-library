package com.company;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Validation {

    int IntValid()  {
        int enter = 0;
        do {
            System.out.print(" -> ");
            Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
            if (sc.hasNextInt()) { // возвращает истинну если с потока ввода можно считать целое число
                enter = sc.nextInt(); // считывает целое число с потока ввода и сохраняем в переменную
            } else {
                System.out.print("Вы ввели не верный формат числа, повторите ввод !");
            }
        } while (enter<=0);
        return enter;
    }

    String StringValid()  {
        String enter = null;
        do {
            System.out.print(" -> ");
            Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
            if (sc.hasNextLine()) { // возвращает истинну если с потока ввода можно считать целое число
                enter = sc.nextLine(); // считывает целое число с потока ввода и сохраняем в переменную
            } else {
                System.out.print("Вы ввели не верный формат строки, повторите ввод !");
            }
        } while (enter == null);
        return enter;
    }

    int UpdateMenuValid(int ending) {
        int enter = 0;
        do {
            System.out.print("Введите номер пункта -> ");
            Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
            if (sc.hasNextInt()) { // возвращает истинну если с потока ввода можно считать целое число
                enter = sc.nextInt(); // считывает целое число с потока ввода и сохраняем в переменную
            } else {
                System.out.println("Вы ввели не верный пункт меню, повторите ввод !");
            }
        } while (enter < 1 || enter > ending);
        return enter;
    }


    String DateStirngValid(){
        String DateStirngValid = null;
        do {
            try {
                System.out.print("Пожалуйста введите дату возврата в фомате ГГГГ-ММ-ДД (например 2017-7-7)");
                String TestDateStirngValid = this.StringValid();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                dateFormat.setLenient(false);
                /*System.out.println("Дата введена верно ! Вы ввели: " + */dateFormat.format(dateFormat.parse(TestDateStirngValid))/*)*/;
                DateStirngValid=TestDateStirngValid;
            } catch (Exception e) {
                System.out.print("Дата введена в не верном формате ! ");
            }
        } while (DateStirngValid == null);
        return DateStirngValid;
    }

}
